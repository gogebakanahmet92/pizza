FROM ubuntu:18.04

MAINTAINER ahmet "gogebakanahmet92@gmail.com"

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev

COPY ./requirements.txt /app/requirements.txt


WORKDIR /app

RUN pip3 install -r requirements.txt

COPY . /app

EXPOSE 8000

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV LANGUAGE=C.UTF-8

ENV DB_URI=sqlite:////app/flask-backend/pizza.sqlite

CMD ["python3" , "main.py"]

